import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

import java.net.*;
import java.io.*;

public class Master {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		EchoClient echoClient = new EchoClient();	
		echoClient.startConnection("127.0.0.1", 5555);
		
		String input;
		while (!(input = sc.nextLine()).equals(".")) {
			echoClient.sendMessage(input);
		}

		sc.close();
		echoClient.sendMessage(".");
		echoClient.stopConnection();
	}
}

class EchoClient {
    private Socket clientSocket;
	private PrintWriter out;
	private BufferedReader in;
    
    public void startConnection(String ip, int port) {
		try {
			clientSocket = new Socket(ip, port);
			out = new PrintWriter(clientSocket.getOutputStream(), true);
			ServerConnection serverConnection = new ServerConnection(clientSocket);
			(new Thread(serverConnection)).start();

		} catch (IOException e) {
			//TODO: handle exception
		}
	}

	public void sendMessage(String msg) {
		try {
			out.println(msg);
		} catch (Exception e) {
			//TODO: handle exception
		}
	}

	public void stopConnection() {
		try {
			in.close();
			out.close();
			clientSocket.close();
		} catch (IOException e) {
			//TODO: handle exception
		}
	}
}

class ServerConnection implements Runnable{
	private Socket server;
    private BufferedReader in;

    public ServerConnection(Socket s) throws IOException {
        server = s;
        in = new BufferedReader(new InputStreamReader(server.getInputStream()));
	}
	
    @Override
    public void run() {
        try {
            while (true) {
                String serverResponse = in.readLine();

                if (serverResponse.equals(".")) break;

                System.out.println(serverResponse);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}