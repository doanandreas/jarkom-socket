import java.net.*;
import java.io.*;
import job.Job;

public class ClientHandler extends Thread {
    private PrintWriter out;
    private BufferedReader in;
    private Socket clientSocket;
    private String idClient;
    private JobHandler jobHandler;
    private JobTypeHandler jobTypeHandler = new JobTypeHandler();

    public ClientHandler(Socket socket, JobHandler jobHandler) {
        this.clientSocket = socket;
        this.jobHandler = jobHandler;
        this.idClient = socket.getInetAddress() + ":" + socket.getPort();
        this.jobHandler.addClient(this.idClient, this);
    }

    public void run() {
        try {
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            out.println(this.getHelpMessage());
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                if(".".equals(inputLine)) {
                    out.println("> Connection closed");
                    break;
                    
                } else if ("help".equals(inputLine)) {
                    out.println(this.getHelpMessage());

                } else if ("set_fcfs".equals(inputLine)) {
                    this.jobHandler.setQueueFcfs();
                    out.println("> Queueing set to FCFS");

                } else if ("set_lcfs".equals(inputLine)) {
                    this.jobHandler.setQueueLcfs();
                    out.println("> Queueing set to LCFS");

                } else if ("q_status".equals(inputLine)) {
                    out.println(this.jobHandler.getQueueStatus());

                } else {
                    Job job = this.jobTypeHandler.createJob(inputLine, this.idClient);
                    if (job == null) {
                        out.println("> Invalid job. Enter 'help' for list of valid commands and jobs.");
                        this.sendJobResult("Status of recent job input: Failed");
                    } else {
                        job.setStatus("Pending");
                        this.sendJobResult(String.format("Status job of %s %d: %s", job.getName(), job.getFibonum(), job.getStatus()));
                        this.jobHandler.addJob(job);
                    }
                }

                System.out.printf("[CLIENT %s:%s]: %s\n", clientSocket.getInetAddress(), clientSocket.getPort(), inputLine);
            }

            in.close();
            out.close();
            this.jobHandler.removeClient(idClient);
            clientSocket.close();
        } catch (IOException e) {
            //TODO: handle exception
        }
    }

    public void sendJobResult(String result) {
        this.out.println(result);
    }

    public String getHelpMessage() {
        return "Commands:\nfib n:      fibonnaci for number n\nfibdp n:    fibonnaci (DP optimized) for number n\nset_fcfs:   sets job queueing to FCFS\nset_lcfs:   sets job queueuing to LCFS\nq_status:   gets queue status of worker job queueing\nhelp:       print this help message";
    }
}