import java.util.LinkedList; 
import java.util.Queue;
import java.util.Stack;
import java.util.HashMap;
import job.Job;

public class JobHandler extends Thread {

    private Queue<Job> jobQueue;
    private Stack<Job> jobStack;
    private HashMap<String, ClientHandler> hashClient;
    private boolean isFCFS;

    public JobHandler () {
        this.jobQueue = new LinkedList<Job>();
        this.jobStack = new Stack<Job>();
        this.hashClient = new HashMap<String, ClientHandler>();
        this.isFCFS = false;
    }

    @Override
    public void run() {
        while (true) {
            System.out.printf("");
            if (isFCFS) {
                if(!this.jobQueue.isEmpty()) {
                    Job job = this.jobQueue.remove();
                    job.setStatus("Running");
                    ClientHandler clientHandler = this.hashClient.get(job.getIdClient());
                    clientHandler.sendJobResult((String.format("Status job of %s %d: %s", job.getName(), job.getFibonum(), job.getStatus())));
                    this.execute(job);
                }
            } else {
                if(!this.jobStack.isEmpty()) {
                    Job job = this.jobStack.pop();
                    job.setStatus("Running");
                    ClientHandler clientHandler = this.hashClient.get(job.getIdClient());
                    clientHandler.sendJobResult((String.format("Status job of %s %d: %s", job.getName(), job.getFibonum(), job.getStatus())));
                    this.execute(job);
                }
            }
        }
    }

    public void addJob(Job job) {
        if (isFCFS) {
            this.jobQueue.add(job);
        } else {
            this.jobStack.push(job);
        }
    }

    public void addClient(String idClient, ClientHandler clientHandler) {
        this.hashClient.put(idClient, clientHandler);
    }

    public void removeClient(String idClient) {
        this.hashClient.remove(idClient);
    }

    public void setQueueFcfs() {
        if(!isFCFS) {
            isFCFS = true;
            Stack<Job> tempStack = new Stack<Job>();

            while(!jobStack.isEmpty()) {
                tempStack.push(jobStack.pop());
            }

            while(!tempStack.isEmpty()) {
                jobQueue.add(tempStack.pop());
            }
        }
    }

    public void setQueueLcfs() {
        if(isFCFS) {
            isFCFS = false;

            while(!jobQueue.isEmpty()) {
                jobStack.push(jobQueue.remove());
            }
        }
    }

    public String getQueueStatus() {
        if (isFCFS) {
            return "> Worker Queue Status: FCFS";
        } else {
            return "> Worker Queue Status: LCFS";
        }
    }

    public void execute(Job job) {
        long startTime = System.currentTimeMillis();
        String result = job.execute();
        long endTime = System.currentTimeMillis();
        long elapsedTime = endTime - startTime;

        String output = String.format(
            "> The result of %s %d is %s with execution time %d ms.",
            job.getName(), job.getFibonum(), result, elapsedTime);
            
        ClientHandler clientHandler = this.hashClient.get(job.getIdClient());
        clientHandler.sendJobResult(output);
        job.setStatus("Finished");
        clientHandler.sendJobResult((String.format("Status job of %s %d: %s", job.getName(), job.getFibonum(), job.getStatus())));
    }
}