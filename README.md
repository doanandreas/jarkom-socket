# Java Socket Programming - Kelompok B2 Jaringan Komputer A

### What is this program?

Program ini merupakan aplikasi *socket programming* untuk Job Management Systems menggunakan paradigma Master-Worker.

### How to use?

1. Pastikan bahwa Java 8 (atau Java yang lebih baru) terinstall di komputer Anda. Jalankan `java --version` dan `javac --version` pada command prompt/terminal untuk mengecek apakah versi Java anda bisa digunakan atau tidak.
2. Compile file-file java diatas dengan `javac *.java`.
3. Jalankan program *MultiServer.java*, yang berperan sebagai Worker. Jalankan `java MultiServer` pada command prompt/terminal Anda.
4. Buka cmd/terminal baru di direktori yang sama. Jalankan `java Master`, yang berperan sebagai Master yang memberi jobs ke worker (*MultiServer.java*).

    ![photos/screenshot1.png](photos/screenshot1.png)

5. Setelah program Master berjalan, akan muncul panduan aplikasi pada cmd/terminal, seperti gambar diatas. Anda bisa memberi jobs dan command sesuai yang diberikan panduan tersebut.

    ![photos/screenshot2.png](photos/screenshot2.png)

6. Pada program Worker (MultiServer) anda, akan ditampilkan IP address dan port number Master, serta isi pesan yang dikirimkan.