package job;

import java.util.HashMap;

public class JobFiboWithDP extends Job{
    private HashMap<Long, Long> memory;
    private long fib(long n){
        if (memory.containsKey(n)) return memory.get(n);
        if(n<=1){
            return n;
        }
        else{
            long value = fib(n-1) + fib(n-2);
            memory.put(n, value);
            return value;
        }
    }

    public String getName(){
        return this.jobName;
    }

    public String execute(){
        memory = new HashMap<Long, Long>();
        return Long.toString(fib(this.fibonum));
    }
}