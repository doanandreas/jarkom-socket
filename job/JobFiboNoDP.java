package job;

public class JobFiboNoDP extends Job{
    private long fib(long n){
        if(n<=1){
            return n;
        }
        else{
            return fib(n-1) + fib(n-2);
        }
    }

    public String getName(){
        return this.jobName;
    }

    public String execute(){
        String x = Long.toString(fib(this.fibonum));
        return x;
    }
}