package job;

public abstract class Job{

    protected Long fibonum;
    private String idClient;
    protected String jobName;
    private String status;

    public abstract String execute();

    public abstract String getName();

    public void setData(Long l) {
        this.fibonum = l;
    }

    public Long getFibonum() {
        return this.fibonum;
    }

    public void setName(String s){
        this.jobName = s;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public String getIdClient() {
        return this.idClient;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}