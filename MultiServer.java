import java.net.*;
import java.io.*;

public class MultiServer {
    private ServerSocket serverSocket;
    private JobHandler jobHandler = new JobHandler();

    public void start(int port) {
        try {
            serverSocket = new ServerSocket(port);
            this.jobHandler.start();
            while (true) {
                new ClientHandler(serverSocket.accept(), this.jobHandler).start();
            }
        } catch (IOException e) {
            //TODO: handle exception
        }
    }

    public void stop() {
        try {
            serverSocket.close();
        } catch (IOException e) {
            //TODO: handle exception
        }
    }

    public static void main(String[] args) {
		MultiServer server = new MultiServer();
		server.start(5555);
		server.stop();
	}

    
}