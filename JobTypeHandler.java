import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

import job.Job;
import job.JobFiboNoDP;
import job.JobFiboWithDP;
import java.util.*;

public class JobTypeHandler {
    // Command = fibonodp/fibowdp --- Value = fibonacci sequence
    // ex: fibonodp 21
    public Job createJob(String jobInput, String idClient){
        String command;
        String argument;
        
        try {
            List<String> input = Arrays.asList(jobInput.split(" "));
            command = input.get(0);
            argument = input.get(1);
        } catch (NullPointerException e) {
            return null;
        } catch (ArrayIndexOutOfBoundsException f) {
            return null;
        }

        Job j;
        if (command.equals("fib")) {
            j = new JobFiboNoDP();
            j.setName("FIBONODP");
        } else if (command.equals("fibdp")) {
            j = new JobFiboWithDP();
            j.setName("FIBOWDP");
        } else {
            return null;
        }
        
        j.setData(Long.parseLong(argument));
        j.setIdClient(idClient);
        return j;
    }
}